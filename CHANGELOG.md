# Changelog

## v1.2.0 - 2025-01-07

### Added

- Support Laravel v11

## v1.1.1 - 2024-01-13

### Fixed

- Missing ID existence check

## v1.1.0 - 2022-03-17

### Added

- Support image, location, audio, video and file messages
- Throw exception on unsuccessful result

### Changed

- Variable assignment in messages: Change `$message->text` to `$message->setText()`

## v1.0.1 - 2022-03-05

### Fixed

- Autoload Threema MsgApi

## v1.0.0 - 2022-03-05

### Added

- Support Laravel v9
- Support basic and end-to-end IDs
- Support text messages
- Support automatic ID lookup via email and phone
